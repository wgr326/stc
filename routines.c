void isqrt(float *a, int n)
{
	int i;
	
	for (i = 0; i < n; i++)
	{
		a[i] = sqrt(a[i]);
		a[i] = 1.0e0 / a[i];
	}
	
}

